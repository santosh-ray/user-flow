import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';

Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);
Vue.use(require('vue-moment'));
window.axios = axios;



Vue.use(BootstrapVue)
Vue.config.productionTip = false

import VueLoading from 'vuejs-loading-plugin'

Vue.use(VueLoading, {
  dark: true, // default false
  text: 'user-flow', // default 'Loading'
  background: 'rgb(8,1,1)', // set custom background
  classes: ['myclass'] // array, object or string
})

new Vue({
	router,
	render: h => h(App),
}).$mount('#app')

