import Vue from 'vue'
import Router from 'vue-router'

import Home from './view/Home.vue'
import UserList from './view/UserList.vue'


Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: "Home",
      component:Home,
    },
    {
      path: '/userlist',
      name: "UserList",
      component:UserList,
    }
    

    

  ]
  
});


export default router;