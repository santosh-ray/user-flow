# user flow


## File Structure
Within the download you'll find the following directories and files:

```
|-- user-flow
    |-- .gitignore
    |-- README.md
    |-- babel.config.js
    |-- package.json
    |-- public
    |   |-- favicon.ico
    |   |-- index.html
    |-- src
        |-- App.vue
        |-- main.js
        |-- router.js
        |-- assets
        |   |-- logo.png
        |-- components
        |   |-- UserModal.vue
        |-- plugins
        |   |-- bootstrap-vue.js
        |-- views
            |-- Home.vue
            |-- UserList.vue
            
```
# Useful Links
- VueCtkDateTimePicker [here](https://github.com/chronotruck/vue-ctk-date-time-picker?ref=madewithvuejs.com)

